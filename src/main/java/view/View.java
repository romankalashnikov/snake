package view;

import model.Model;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class View extends JFrame {
    private JPanel panel = new JPanel(new BorderLayout());


    public View() {
        super("Snake Game");
        new Frame();
        createGameField();
        setSize(Model.getWIDTH() * Model.getSIZE() + 5, Model.getHEIGHT() * Model.getSIZE() + 100);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setJMenuBar(createMenu());
        setVisible(true);

    }

    private void createGameField() {
        panel.add(new Model());
        panel.setVisible(true);
        add(panel);
    }

    private JMenuBar createMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu game = new JMenu("Game");
        JMenuItem newGame = new JMenuItem("New Game");
        game.add(newGame);
        newGame.addActionListener(e -> {
            if (Model.getGameState()) {
                panel.setVisible(false);
                createGameField();

                Model.setGameState(false);
            }
        });
        JMenuItem closeItem = new JMenuItem("Close");
        fileMenu.add(closeItem);
        JMenuItem closeAllItem = new JMenuItem("Close all");
        fileMenu.add(closeAllItem);
        fileMenu.addSeparator();

        JMenuItem exitItem = new JMenuItem("Exit");
        fileMenu.add(exitItem);
        exitItem.addActionListener(e -> System.exit(0));
        menuBar.add(fileMenu);
        menuBar.add(game);
        return menuBar;
    }
}
