package model;

import controller.Controlls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

public class Model extends JPanel implements ActionListener {
    private static Logger log = Logger.getLogger(Model.class.getName());
    private static final int SIZE = 20;
    private static final int WIDTH = 20;
    private static final int HEIGHT = 20;
    private static final int SPEED = 5;
    private static final int DELAY = 1000;

    private static Snake snake = new Snake(0, 0, 0, 0);
    private static Cherry cherry = new Cherry(Math.abs((int) (Math.random() * getWIDTH() - 1)),
            Math.abs((int) (Math.random() * getHEIGHT() - 1)));

    private Timer timer = new Timer(DELAY / SPEED, this);

    public Model() {
        snake.sX[0] = 8;
        snake.sY[0] = 8;
        snake.setDerection(2);
        snake.setLenght(1);
        timer.start();
        addKeyListener(new Controlls());
        setFocusable(true);
        log.info("Model");
    }

    public static void setGameState(boolean gameState) {
        Model.gameState = gameState;
    }

    private static boolean gameState = false;

    public static boolean getGameState() {
        return gameState;
    }


    public static int getSIZE() {
        return SIZE;
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public static Snake getSnake() {
        return snake;
    }


    private void createField(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, HEIGHT * SIZE, WIDTH * SIZE);
        for (int x = SIZE; x < WIDTH * SIZE; x += SIZE) {
            for (int y = SIZE; y < HEIGHT * SIZE; y += SIZE) {
                g.setColor(Color.BLACK);
                g.drawLine(0, x, WIDTH * SIZE, x);
                g.drawLine(y, 0, y, HEIGHT * SIZE);
            }
        }
    }

    private void createSnakeHeadAndBody(Graphics g) {
        for (int i = 0; i < snake.getLenght(); i++) {
            g.setColor(Color.GREEN);
            g.fillRect(snake.sX[i] * SIZE + 3, snake.sY[i] * SIZE + 3, SIZE - 6, SIZE - 6);
            g.setColor(Color.WHITE);
            g.fillRect(snake.sX[0] * SIZE + 3, snake.sY[0] * SIZE + 3, SIZE - 6, SIZE - 6);
        }
    }

    private void createCherry(Graphics g) {
        g.setColor(Color.RED);
        g.fillOval(cherry.posX * SIZE, cherry.posY * SIZE, SIZE - 6, SIZE - 6);
    }

    private void eatCherry() {
        if (snake.sX[0] == cherry.posX && snake.sY[0] == cherry.posY) {
            cherry.setRandomCherry();
            snake.setLenght(snake.getLenght() + 1);
        }
    }

    private void isGameOver() {
        for (int i = 1; i < snake.getLenght(); i++) {
            if (snake.sX[0] == snake.sX[i] && snake.sY[0] == snake.sY[i]) {
                timer.stop();
                JOptionPane.showMessageDialog(null, "Сам себя");
                log.info("конец игры потому что сам себя коснулся");
                setGameState(true);
            }
        }
        if (snake.sX[0] >= getWIDTH() || snake.sY[0] >= getHEIGHT() || (snake.sX[0] < 0 || snake.sY[0] < 0)) {
            timer.stop();
            JOptionPane.showMessageDialog(null, "Края поля");
            setGameState(true);
            log.info("конец игры потому что  коснулся края поля");
        }
    }

    public void paint(Graphics g) {
        createField(g);
        createCherry(g);
        createSnakeHeadAndBody(g);
        eatCherry();

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        snake.move();
        eatCherry();
        isGameOver();
        repaint();
    }

}







