package model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

class Cherry {
    int posX;
    int posY;

    Cherry(int x, int y){
        posX = x;
        posY = y;
    }
    void setRandomCherry(){
        posX = Math.abs ((int) (Math.random()* Model.getWIDTH() -1));
        posY = Math.abs ((int) (Math.random()*Model.getHEIGHT() -1));
    }

    Image getImageCherry(){
        Image img = null;
        try {
            img = ImageIO.read(new File(Snake.class.getClassLoader().getResource("cherry.jpg").getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }
}
